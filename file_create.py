#defining a function to create a file
def create_file(name,extension):
	#giving the path of the file
	path="C:/Users/hkakarla/Desktop/{0}{1}".format(name,extension)
	#creating a file
	file=open(path,"x")
	file.write("this is a new python file")
	#closing the open file
	file.close()
	#returning the path of the file
	return "this is a new python file{0}".format(path)
#calling the function 
print(create_file(name="hii",extension=".txt"))
